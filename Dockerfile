FROM gcr.io/kaniko-project/executor:debug

COPY certs/* /kaniko/ssl/certs/

CMD ["echo", "This is a 'Purpose-Built Container', It is not meant to be ran this way. Please review the documentation on usage."]

